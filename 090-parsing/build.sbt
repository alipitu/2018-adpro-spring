name := "ADPRO exercises Parsing"

version := "0.0"

scalaVersion := "2.12.5"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.4" 
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"
